public class Llamadas {
    public static void main(String[] args) {
        System.out.println("l, u, 13:30, 10 => " + cobrar('l', 'u', "13:30", 10));
        System.out.println("L, u, 13:30, 10 => " + cobrar('L', 'u', "13:30", 10));
        System.out.println("L, U, 13:30, 10 => " + cobrar('L', 'U', "13:30", 10));
        System.out.println("l, U, 13:30, 10 => " + cobrar('l', 'U', "13:30", 10) + "\n");

        System.out.println("m, a, 03:30, 10 => " + cobrar('m', 'a', "03:30", 10));
        System.out.println("M, a, 03:30, 10 => " + cobrar('M', 'a', "03:30", 10));
        System.out.println("M, A, 03:30, 10 => " + cobrar('M', 'A', "03:30", 10));
        System.out.println("m, A, 03:30, 10 => " + cobrar('m', 'A', "03:30", 10) + "\n");

        System.out.println("j, u, 13:30, 10 => " + cobrar('j', 'u', "13:30", 10));
        System.out.println("J, u, 13:30, 10 => " + cobrar('J', 'u', "13:30", 10));
        System.out.println("J, U, 13:30, 10 => " + cobrar('J', 'U', "13:30", 10));
        System.out.println("j, U, 13:30, 10 => " + cobrar('j', 'U', "13:30", 10) + "\n");

        System.out.println("v, i, 07:30, 10 => " + cobrar('v', 'i', "07:30", 10));
        System.out.println("V, i, 19:30, 10 => " + cobrar('V', 'i', "19:30", 10));
        System.out.println("V, I, 07:30, 10 => " + cobrar('V', 'I', "07:30", 10));
        System.out.println("v, I, 19:30, 10 => " + cobrar('v', 'I', "19:30", 10) + "\n");

        System.out.println("s, a, 19:30, 10 => " + cobrar('s', 'a', "19:30", 10));
        System.out.println("S, a, 07:30, 10 => " + cobrar('S', 'a', "07:30", 10));
        System.out.println("S, A, 12:30, 10 => " + cobrar('S', 'A', "12:30", 10));
        System.out.println("s, A, 19:30, 10 => " + cobrar('s', 'A', "19:30", 10) + "\n");
    }

    public static int isDay(String dia) {
        String[] luVi = { "lu", "ma", "mi", "ju", "vi", "sa", "do" };

        for (int i = 0; i < luVi.length; i++) {
            if (luVi[i].equals(dia))
                return i;
        }

        return -1;
    }

    public static boolean isLuVi(String dia) {
        int index = isDay(dia);

        if (index >= 0 && index <= 4)
            return true;
        else
            return false;
    }

    private static int[] parseHora(String hora) {
        String[] horaSplit = hora.split(":", 0);
        int[] horaMin = { Integer.parseInt(horaSplit[0]), Integer.parseInt(horaSplit[1]) };

        return horaMin;
    }

    public static float cobrar(char dia1, char dia2, String horaInicioStr, int minutos) {
        String dia = "" + dia1 + dia2;
        dia = dia.toLowerCase();
        int[] horaInicio = parseHora(horaInicioStr);
        float tarifa1 = 0.4f;
        float tarifa2 = 0.25f;
        float tarifa3 = 0.15f;

        if (isLuVi(dia)) {
            if ((horaInicio[0] < 8 && horaInicio[1] < 0) || (horaInicio[0] > 18 && horaInicio[0] > 0))
                return minutos * tarifa2;
            else
                return minutos * tarifa1;
        } else {
            return minutos * tarifa3;
        }
    }
}
