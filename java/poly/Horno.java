public class Horno {
    public static void main(String[] args) {
        System.out.println(332 + " => " + temperatura("332"));
        System.out.println(147 + " => " + temperatura("147"));
        System.out.println(412 + " => " + temperatura("412"));
        System.out.println(612 + " => " + temperatura("612"));
        System.out.println(673 + " => " + temperatura("673"));
    }

    private static boolean validateNumbers(String tempString) {
        return tempString.contains("1") || tempString.contains("4") || tempString.contains("7");
    }

    public static String temperatura(String tempString) {
        int temp = Integer.parseInt(tempString);

        if (temp <= 0 || temp > 999)
            return "La temperatura debe estar entre 0 y 999 grados!!!";

        if (validateNumbers(tempString)) {
            int tempLower = temp;
            int tempUpper = temp;
            String tempLowerString = Integer.toString(tempLower);
            String tempUpperString = Integer.toString(tempUpper);

            while (validateNumbers(tempLowerString) || validateNumbers(tempUpperString)) {
                if (validateNumbers(tempLowerString)) {
                    tempLower--;
                    tempLowerString = Integer.toString(tempLower);
                }
                if (validateNumbers(tempUpperString)) {
                    tempUpper++;
                    tempUpperString = Integer.toString(tempUpper);
                }
            }

            return tempLowerString + ", " + tempUpperString;
        } else {
            return tempString;
        }
    }
}
