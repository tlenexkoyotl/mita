public class Batman {
    public static void main(String[] args) {
        System.out.println("La dirección es Pensilvania #" + validarDireccion());
    }

    private static int[] digitos(String direccion) {
        String[] digits = direccion.split("", 0);
        int[] digitos = { Integer.parseInt(digits[0]), Integer.parseInt(digits[1]), Integer.parseInt(digits[2]),
                Integer.parseInt(digits[3]) };

        return digitos;
    }

    private static boolean suman27(String direccion) {
        int[] digs = digitos(direccion);

        return (digs[0] + digs[1] + digs[2] + digs[3]) == 27;
    }

    private static boolean regla1(String direccion) {
        int[] digs = digitos(direccion);

        for (int i = 0; i < digs.length; i++) {
            for (int j = 0; j < digs.length; j++) {
                if (digs[i] == digs[j] && i != j)
                    return false;
            }
        }

        return true;
    }

    private static boolean regla2(String direccion) {
        int[] digs = digitos(direccion);

        return digs[0] == (digs[2] * 3);
    }

    private static boolean regla3(String direccion) {
        return Integer.parseInt(direccion) % 2 != 0;
    }

    private static boolean validateNumbers(String tempString) {
        return tempString.contains("1") || tempString.contains("4") || tempString.contains("7");
    }

    private static boolean validar(String dir) {
        return suman27(dir) && regla1(dir) && regla2(dir) && regla3(dir);
    }

    public static String validarDireccion() {
        String dirString = "1234";
        int dir = Integer.parseInt(dirString);
        int[] digs = digitos(dirString);

        while (!validar(dirString)) {
            dir++;

            dirString = Integer.toString(dir);
        }

        return dirString;
    }
}
